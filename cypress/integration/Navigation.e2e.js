describe('App E2E', () => {
    it('should navigate using card', () => {
        cy.visit('/');
        cy.get('ion-title')
            .should('have.text', 'Clients');
        cy.get('ion-card').click();
        cy.get('ion-title#oders')
            .should('have.text', 'Orders');
        cy.get('ion-card').first().click();
        cy.get('ion-title#OrderDetail')
            .should('have.text', 'Order details');
    });
});