import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonList, IonCard, IonCardHeader, IonCardTitle, IonCardContent, IonLabel } from '@ionic/react'
import React from 'react'
import { GetClients } from '../Service/ClientService'
import { RouteComponentProps } from 'react-router'
// #region Client View

/**
 * fonction retournant une liste d'éléments visuel comportant les information des clients
 */
function GetClientsviews(): any {
  const [clientInfo, SetClientInfo] = React.useState([Object])

  React.useEffect(() => {
    GetClients().then(data => {
      return SetClientInfo(data || [Object])
    })
  })

  const clients = clientInfo.map((client: any) => (
    <IonCard routerLink={'orders/' + client.id} routerDirection="forward">
      <IonCardHeader>
        <IonCardTitle>{client.name}</IonCardTitle>
      </IonCardHeader>
      <IonCardContent>
        <IonLabel>Mail : {client.mail}</IonLabel>
        <IonLabel>Address : {client.adress}</IonLabel>
      </IonCardContent>
    </IonCard>
  ))
  return (clients)
}
// #endregion

const Home: React.FC<RouteComponentProps> = (props) => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle id="title">Clients</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding">
        <IonList id="ClientList">
          <GetClientsviews />
        </IonList>
      </IonContent>
    </IonPage>
  )
}

export default Home
