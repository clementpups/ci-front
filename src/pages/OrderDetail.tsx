import { IonPage, IonHeader, IonToolbar, IonTitle, IonContent, IonList, IonCard, IonCardHeader, IonCardTitle, IonCardContent, IonLabel, IonBackButton } from '@ionic/react'

import React from 'react'
import { RouteComponentProps } from 'react-router'
import { GetOrderDetails } from '../Service/OrderService'

interface OrderDetailsPageProps extends RouteComponentProps<{
  OrderId: string;
}> { };

function GetOrderDietailViews(OrderId: string): any {
  const [orderDetailsInfo, SetorderDetailsInfo] = React.useState([Object])

  React.useEffect(() => {
    GetOrderDetails(OrderId).then(data => {
      return SetorderDetailsInfo(data || [Object])
    })
  })

  const orders = orderDetailsInfo.map((orderDetail: any) => (
    <IonCard>
      <IonCardHeader>
        <IonCardTitle>{orderDetail.name}</IonCardTitle>
      </IonCardHeader>
      <IonCardContent>
        <IonLabel>Prix : {orderDetail.retailPrice}</IonLabel>
        <IonLabel>Quantité : {orderDetail.qty}</IonLabel>
      </IonCardContent>
    </IonCard>
  ))

  return (orders)
}

const OrderDetails: React.FC<OrderDetailsPageProps> = ({ match }) => {
  console.log(match.params.OrderId)
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle id="OrderDetail">Order details</IonTitle>
          <IonBackButton />
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding">
        <IonList>
          {GetOrderDietailViews(match.params.OrderId)}
        </IonList>
      </IonContent>
    </IonPage>
  )
}

export default OrderDetails
