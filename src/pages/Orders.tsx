import { IonPage, IonHeader, IonToolbar, IonTitle, IonContent, IonList, IonCard, IonCardHeader, IonCardTitle, IonCardContent, IonLabel, IonBackButton } from '@ionic/react'

import React from 'react'
import { RouteComponentProps } from 'react-router'
import { GetClientOrders } from '../Service/ClientService'

interface OrdersPageProps extends RouteComponentProps<{
  UserId: string;
}> { }

function GetOrderViews(UserId: string): any {
  const [orderInfo, SetOrderInfo] = React.useState([Object])

  React.useEffect(() => {
    GetClientOrders(UserId).then(data => {
      return SetOrderInfo(data || [Object])
    })
  })

  const orders = orderInfo.map((order: any) => (
    <IonCard routerLink={'/orderdetail/' + order.id} routerDirection="forward">
      <IonCardHeader>
        <IonCardTitle>{order.name}</IonCardTitle>
      </IonCardHeader>
      <IonCardContent>
        <IonLabel>date de commande : {order.date}</IonLabel>
      </IonCardContent>
    </IonCard>
  ))

  return (orders)
}

const Orders: React.FC<OrdersPageProps> = ({ match }) => {
  console.log(match.params.UserId)
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle id="oders">Orders</IonTitle>
          <IonBackButton />
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding">
        <IonList>
          {GetOrderViews(match.params.UserId)}
        </IonList>
      </IonContent>
    </IonPage>
  )
}

export default Orders
