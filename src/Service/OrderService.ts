import { CommandDetail } from './RouteManager'
import { GetApi } from './ApiService'

export function GetOrderDetails (OrderId : string) : Promise<any> {
  return GetApi(CommandDetail + '/' + OrderId)
}
