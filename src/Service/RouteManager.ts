var ApiRadical = 'https://localhost:5001'

function GetRadical () : string {
  if (process.env.REACT_APP_API_URL !== undefined) {
    ApiRadical = process.env.REACT_APP_API_URL
  }
  return ApiRadical
}

/// //
/// la liste des clients
/// le détail des client avec l'info de client id
/// //
export const ClientAPI : string = GetRadical() + '/client'

/// //
/// le detail d'une commande avec l'info de commade Id
/// //
export const CommandDetail : string = GetRadical() + '/order'
