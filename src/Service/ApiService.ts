export function GetApi (ApiAdress : string) : Promise<any> {
  // appel api

  return fetch(ApiAdress)
    .then(res => { return res.json() })
    .then((data) => {
      // console.log(data)
      return data
    })
    .catch(console.log)
}
