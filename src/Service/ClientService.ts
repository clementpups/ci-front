import { ClientAPI } from './RouteManager'
import { GetApi } from './ApiService'

export function GetClients () : Promise<any> {
  return GetApi(ClientAPI)
}

export function GetClientOrders (ClientId : string) : Promise<any> {
  return GetApi(ClientAPI + '/' + ClientId)
}
